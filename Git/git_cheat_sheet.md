# GET STARTED WITH GIT

## INSTALL GIT
Download and install software from this website
https://git-scm.com/download

## CONFIG
Open terminal run the commands with your details.
    
    git config --global user.name "Trond Egil Anuglen"
    git config --global user.email "trond@anuglen.com"
    

Extra git config in terminal if you want to use a spesial editor. Remember to replace with path to your favorite editor.

    git config --global diff.tool gvimdiff
    git config --global core.editor "'C:\Program Files (x86)\Vim\vim81\gvim.exe' -f -i NONE"


